import java.util.Scanner;

public class Volumenberechnung {
	
	public static double wuerfel(double zahl1 ) {
		return zahl1 * zahl1 * zahl1;
	}
	public static double quader(double zahl1, double zahl2, double zahl3) {
		return zahl1 * zahl2 * zahl3;
	}
	public static double pyramide(double zahl1 , double zahl2) {
		return zahl1 * zahl1 * zahl2 / 3;
	}
	public static double kugel(double zahl1) {
		return 1.33333333 * (zahl1 * zahl1 * zahl1) * 3.14;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in); 
		double a;
		double a2;
		double b;
		double c;
		double erg_wuerfel;
		double erg_quader;
		double a3, h;
		double erg_pyramide;
		double r;
		double erg_kugel;
		
		System.out.println("Wie gro� ist die Seite des W�rfels");
		a = tastatur.nextDouble();
		erg_wuerfel = wuerfel(a);
		System.out.printf("%.2f", erg_wuerfel);
		System.out.println("\nWie gro� ist die erste seite des Quaders? ");
		a2 = tastatur.nextDouble();
		System.out.println("Wie gro� ist die zweite Seite des Quaders? ");
		b = tastatur.nextDouble();
		System.out.println("Wie hoch ist der Quader? ");
		c = tastatur.nextDouble();
		erg_quader = quader(a2, b, c);
		System.out.printf("%.2f", erg_quader);
		System.out.println("\nWie gro� ist die erste Seite der Pyramide");
		a3 = tastatur.nextDouble();
		System.out.println("Wie hoch ist die Pyramide?");
		h = tastatur.nextDouble();
		erg_pyramide = pyramide(a3, h);
		System.out.printf("%.2f",erg_pyramide);
		System.out.println("\nWie ist der Radius der Kugel?");
		r = tastatur.nextDouble();
		erg_kugel = kugel(r);
		System.out.printf("%.2f",erg_kugel);
		
		
	}

}
