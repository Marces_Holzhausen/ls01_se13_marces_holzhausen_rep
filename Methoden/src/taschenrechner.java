import java.util.Scanner;

public class taschenrechner {
	public static double addition(double a, double b) {
		return a+b;
	}
	public static double subtraktion (double a, double b) {
		return a - b;
	}
	public static double multiplikation(double a, double b) {
		return a * b;
	}
	public static double division(double a, double b) {
		return a / b;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		double zahl1;
		double zahl2;
		double erg;
		boolean nochmal = false;
		String rechenart;
		
		do {
		System.out.println("Mit welcher Zahl soll gerechnet werden?");
		zahl1 = tastatur.nextDouble();
		System.out.println("Wie lautet die zweite Zahl?");
		zahl2 = tastatur.nextDouble();
		System.out.println("Mit welcher Rechenart wollen sie rechner(addition, subtraktion, multiplikation oder division)? ");
		rechenart = tastatur.next();
		switch(rechenart) {
		case "addition":
			erg =addition(zahl1, zahl2);
			System.out.println("Das Ergerbnis der addition lautet: ");
			System.out.printf("%.2f", erg);
			nochmal = true;
			break;
		case "subtraktion":
			erg = subtraktion(zahl1, zahl2);
			System.out.println("\nDas Ergerbnis der subtraktion lautet: ");
			System.out.printf("%.2f", erg);
			nochmal = true;
			break;
		case "multiplikation":
			erg = multiplikation(zahl1, zahl2);
			System.out.println("\nDas Ergerbnis der multiplikation lautet: ");
			System.out.printf("%.2f", erg);
			nochmal = true;
			break;
		case "division":
			erg = division(zahl1, zahl2);
			System.out.println("\nDas Ergerbnis der division lautet: " );
			System.out.printf("%.2f", erg);
			nochmal = true;
			break;
		default:
			System.out.println("Falsche Eingabe");
			nochmal = false;
			break;
			
		}
		} while (nochmal != true);
		
		//erg =addition(zahl1, zahl2);
		//System.out.println("Das Ergerbnis der addition lautet: ");
		//System.out.printf("%.2f", erg);
		//erg = subtraktion(zahl1, zahl2);
		//System.out.println("\nDas Ergerbnis der subtraktion lautet: ");
		//System.out.printf("%.2f", erg);
		//erg = multiplikation(zahl1, zahl2);
		//System.out.println("\nDas Ergerbnis der multiplikation lautet: ");
		//System.out.printf("%.2f", erg);
		//erg = division(zahl1, zahl2);
		//System.out.println("\nDas Ergerbnis der division lautet: " );
		//System.out.printf("%.2f", erg);
		
		
		
		

	}

}
