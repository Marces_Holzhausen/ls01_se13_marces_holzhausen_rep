
public class HelloWorld {

	public static void main(String[] args) {
		/* dies ist 
		 * ein mehrzeiliger
		 * Kommentar
		 */
		
		//Arbeit mit Variabeln
		//Deklaration einern Ganzzahligen Variablen "zahl1"
		//Initialtisierung mit dem Wert 5
		int zahl1 = 5;
		int zahl2 = 18;
		String ausgabe = "f�nf plus achtzuehn ist gleich ";
		int ergebnis;
		ergebnis = zahl1 + zahl2;
		System.out.println(ausgabe + ergebnis);
		System.out.println("Die variable zahl1 hat den Wert:" + zahl1);
		System.out.println("zahl1");
		System.out.println(zahl1+zahl2);
		System.out.println(zahl1+" "+zahl2);
		// print erzeugt Ausgabe ohne Zeilenumbruch 
		System.out.print("Heute ist Montag");
		// println sowie \n erzeugt Ausgabe mit Zeilenumbruch
		System.out.println(" \nHeute haben wir Sport");
	}

}
