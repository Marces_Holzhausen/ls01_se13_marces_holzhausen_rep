import java.util.Scanner;

public class Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		
		double zahl1;
		
		System.out.println("Gib eine Zahl ein");
		zahl1 = tastatur.nextDouble();
		
		if (zahl1 < 0) {
			System.out.println("negativ");
		}
		else {
			System.out.println("positiv");
		}
		tastatur.close();

	}

}
