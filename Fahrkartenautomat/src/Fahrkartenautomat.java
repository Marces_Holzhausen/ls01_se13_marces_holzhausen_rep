﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static double einzelfahrkarte2(int a, double b) {
		return a * b;
	}
	public static double tagesticket2(int a, double b) {
		return a * b;
	}
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag = 0; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       double einzelfahrkarte = 1.50;
       double tagesticket = 4.75;
       double wochenticket = 10.50;
       double monatskarte = 30.90;
       String fahrkartenauswahl;
       int fahrkartenanzahl;
       boolean loop1 = true;



       // Geldeinwurf
       // -----------
       System.out.println("Welche fahrkarte wollen Sie haben Sie haben die Auswahle zwischen Einzelfahricket, Tagesticket, Wochenticket, Monatskarte");
       fahrkartenauswahl = tastatur.next();
       System.out.println("Wie viele Fahrkarten wollen Sie? ");
       fahrkartenanzahl = tastatur.nextInt();
       do {
    	   switch(fahrkartenauswahl) {
    	   
    	   case "Einzelfahrkarte":
    		   System.out.println("Sie müssen "+ einzelfahrkarte2(fahrkartenanzahl , einzelfahrkarte) + " zahlen");
    		   zuZahlenderBetrag = einzelfahrkarte2(fahrkartenanzahl , einzelfahrkarte);
    		   loop1 = false;
    		   break;
    		   
    	   case "Tagesticket":
    		   System.out.println("Sie müssen " + tagesticket2(fahrkartenanzahl , tagesticket)+ " zahlen");
    		   zuZahlenderBetrag = tagesticket2(fahrkartenanzahl , tagesticket);
    		   loop1 = false;
    		   break;
    		   
    		   
           
           }
    	   
       }while (loop1 != false);
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}